from django.conf.urls import url
from django.contrib import admin
from btc_app.views import last_rates


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^last-rates/$', last_rates),
]
