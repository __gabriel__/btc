from django.apps import AppConfig


class BtcAppConfig(AppConfig):
    name = 'btc_app'
