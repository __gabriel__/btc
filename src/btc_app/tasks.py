from __future__ import absolute_import, unicode_literals
import json
import requests
from celery import shared_task
from .models import Rate


@shared_task(name='fetch_rate')
def fetch_rate():
    fetched = requests.get('http://api.coindesk.com/v1/bpi/currentprice.json')
    data = fetched.json()
    date = data['time']['updatedISO']
    value = data['bpi']['USD']['rate_float']
    if Rate.objects.filter(fetched=date).exists():
        return 0
    else:
        Rate(value=value, fetched=date).save()
        return 1
