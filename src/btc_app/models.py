from django.db import models


class Rate(models.Model):
    value = models.DecimalField(max_digits=8, decimal_places=4)
    fetched = models.DateTimeField()

    def __str__(self):
        return '<USD Rate at UTC: {}>'.format(self.fetched)
