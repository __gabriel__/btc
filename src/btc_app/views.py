from django.shortcuts import render
from django.http import JsonResponse
from datetime import datetime, timedelta
from .models import Rate


def last_rates(request):
    delta = datetime.utcnow() - timedelta(minutes=30)
    data = Rate.objects.filter(fetched__gte=delta).order_by('-id')
    data = data.values('value', 'fetched')
    data = data[:30][::-1]
    return JsonResponse(data, safe=False)
