BTC README

1)clone repo

2)virtualenv --python python3.5 btc_env

3)activate env

4)pip install -r requirements.txt

5)sudo apt-get install rabbitmq-server

6)in new terminal with env on:
    sudo rabbitmq-server ( to stop--> sudo rabbitmqctl stop) // broker for celery

7) in new terminal with env on and in src folder:
    celery -A btc worker -l info   //worker to perform tasks

8)in new terminal with env on and in src folder:
    celery -A btc beat    // process taking care of queueing tasks

9)in src folder with env on:
    python manage.py runserver

10) GET  /last-rates

11)GET /admin
    login: asd
    pass: asdasdqwe
